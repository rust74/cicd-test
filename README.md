# cicd-test

# Status

[![pipeline status](https://gitlab.com/rust74/cicd-test/badges/main/pipeline.svg)](https://gitlab.com/rust74/cicd-test/-/commits/main)

## Getting started

This Project uses [asdf-vm](https://asdf-vm.com/) to manage the runtime-environment. It is recommended to install asdf-vm to build the project locally as intended.
To build the project locally use the following commands:

```bash
# add the rust plugin with asdf
asdf plugin add rust
asdf plugin add rust-analyzer

# install the dependecies from .tools-version
asdf install 

# build the rust app using cargo
cargo build

# run the app
cargo run
```
